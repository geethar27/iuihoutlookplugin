﻿namespace IUIHSkin
{
    interface IMaterialControl
    {
        int Depth { get; set; }
        IUIHSkinManager SkinManager { get; }
        MouseState MouseState { get; set; }

    }

    public enum MouseState
    {
        HOVER,
        DOWN,
        OUT
    }
}
