﻿using System.ComponentModel;
using System.Windows.Forms;

namespace IUIHSkin.Controls
{
    public sealed class MaterialDivider : Control, IMaterialControl
    {
        [Browsable(false)]
        public int Depth { get; set; }
        [Browsable(false)]
        public IUIHSkinManager SkinManager { get { return IUIHSkinManager.Instance; } }
        [Browsable(false)]
        public MouseState MouseState { get; set; }
        
        public MaterialDivider()
        {
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            Height = 1;
            BackColor = SkinManager.GetDividersColor();
        }
    }
}
