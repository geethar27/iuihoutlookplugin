﻿using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Timers;
using System.Windows;
using Office = Microsoft.Office.Core;
using Outlook = Microsoft.Office.Interop.Outlook;


namespace IUIH
{
    [ComVisible(true)]
    public class Ribbon : Office.IRibbonExtensibility
    {
        public Office.IRibbonUI ribbon;
        public  PostEmailDetails postEmailDetails;

        private string registryPath = ConfigurationManager.AppSettings["RegistryPath"].ToString();
        private string ServiceURL = ConfigurationManager.AppSettings["ServiceURL"].ToString();
        private string authUserName = ConfigurationManager.AppSettings["AuthUserName"].ToString();
        private string authPassword = ConfigurationManager.AppSettings["AuthPassword"].ToString();
        public static int timerInterval = Convert.ToInt32(ConfigurationManager.AppSettings["TimerInterval"].ToString());


        #region COnSTRUCTOR
        public Ribbon()
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey(registryPath, true);

            if (key == null)
                key = Registry.CurrentUser.CreateSubKey(registryPath, RegistryKeyPermissionCheck.Default);

            if (!string.IsNullOrEmpty(Convert.ToString(key.GetValue("UserName"))) && !string.IsNullOrEmpty(Convert.ToString(key.GetValue("Password"))))
            {
                postEmailDetails = new PostEmailDetails();
                postEmailDetails.userName = key.GetValue("UserName").ToString().Trim();
                postEmailDetails.loginUniqueKey = key.GetValue("LoginUniqueKey").ToString().Trim();
                key.Close();

                System.Timers.Timer myTimer = new System.Timers.Timer();
                myTimer.Elapsed += new ElapsedEventHandler(OnTimer);
                myTimer.Interval = (timerInterval * 60 * 1000);  
                myTimer.Enabled = true;              
                myTimer.AutoReset = false;                
            }

           
        }  

        public Ribbon(string st)
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey(registryPath, true);

            if (key == null)
                key = Registry.CurrentUser.CreateSubKey(registryPath, RegistryKeyPermissionCheck.Default);

            if (!string.IsNullOrEmpty(Convert.ToString(key.GetValue("UserName"))) && !string.IsNullOrEmpty(Convert.ToString(key.GetValue("Password"))))
            {
                postEmailDetails = new PostEmailDetails();
                postEmailDetails.userName = key.GetValue("UserName").ToString().Trim();
                postEmailDetails.loginUniqueKey = key.GetValue("LoginUniqueKey").ToString().Trim();
                key.Close();
            }
        }
        #endregion

        #region TIMER
        public static void OnTimer(Object source, ElapsedEventArgs e)
        {
            GC.Collect();
            Ribbon obj = new Ribbon("");
            if (obj.postEmailDetails.loginUniqueKey != null)
            {
                obj.SyncCalenderEvents(true);
            }

            System.Timers.Timer theTimer = (System.Timers.Timer)source;
            theTimer.Interval = (timerInterval * 60 * 1000);
            theTimer.Enabled = true;
        }
        #endregion

        #region API COMMON CALL
        private dynamic GenerateServiceCall(string serviceMethod, string serviceParameter,string requestMethod)
        {
            dynamic jsonReturn = null;

            try
            {
                HttpWebRequest request = null;
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                if (!string.IsNullOrEmpty(serviceParameter) && requestMethod.ToUpper() == "GET")
                    request = (HttpWebRequest)WebRequest.Create(ServiceURL + serviceMethod + "/" + serviceParameter);
                else
                    request = (HttpWebRequest)WebRequest.Create(ServiceURL);

                request.Credentials = new NetworkCredential("username", "password");
                string credidentials = authUserName + ":" + authPassword; // "demo" + ":" + "dc4811b90ebe";
                var authorization = Convert.ToBase64String(Encoding.Default.GetBytes(credidentials));
                request.Headers["Authorization"] = "Basic " + authorization;
           

                if (requestMethod.ToUpper() == "POST")
                {
                    var data = Encoding.UTF8.GetBytes(serviceMethod);

                    request.Method = "POST";
                    request.ContentType = "application/x-www-form-urlencoded";
                    request.ContentLength = data.Length;
                    request.KeepAlive = true;
                    request.Accept = "*/*";

                    using (var stream = request.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                        stream.Close();
                    }
                }

                var response = (HttpWebResponse)request.GetResponse();

                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                string stringResponseString = Convert.ToString(responseString);

                dynamic returnResponse = stringResponseString.Contains("<div") ? null : JsonConvert.DeserializeObject(responseString);

                jsonReturn = returnResponse;

                return jsonReturn;
            }
            catch (Exception err)
            {               
                if(serviceParameter == "")
                    System.Windows.MessageBox.Show(err.Message, "IUIH", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return jsonReturn;
        }
        #endregion

        private List<EventDetails> SyncCalenderEvents(bool isTimercall)
        {
            List<EventDetails> eventDetailsList = new List<EventDetails>();
            try
            {
                //Get company Name from service                
                dynamic returnValue = GenerateServiceCall("action=get_events&" + "userId=" + postEmailDetails.loginUniqueKey, "timer", "POST"); //

                if (returnValue == null)
                {
                    if (!isTimercall)
                    {
                        System.Windows.MessageBox.Show("Server not responding. Please try later.", "IUIH", MessageBoxButton.OK, MessageBoxImage.Information);
                    }

                    return eventDetailsList;
                }

                bool bStatuss = (((Newtonsoft.Json.Linq.JValue)returnValue.status).Value == null) ? false : Convert.ToBoolean(((Newtonsoft.Json.Linq.JValue)returnValue.status).Value);

                Outlook.Application outlookApp = new Outlook.Application(); // creates new outlook app
               
                bool bDeletedStatuss = (((Newtonsoft.Json.Linq.JValue)returnValue.deletedEventStatus).Value == null) ? false : Convert.ToBoolean(((Newtonsoft.Json.Linq.JValue)returnValue.deletedEventStatus).Value);
                if (bDeletedStatuss)
                {
                    //delete the events
                    var deletedEvents = (Newtonsoft.Json.Linq.JArray)returnValue.deletedEvents;
                    EventDetails[] deletedEventList = deletedEvents.ToObject<EventDetails[]>();
                    if (deletedEventList.Length >= 1)
                    {                       
                        foreach (EventDetails evtObj in deletedEventList)
                        {
                            Outlook.MAPIFolder calendar = outlookApp.Session.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderCalendar);

                            Outlook.Items calendarItems = calendar.Items;
                            Outlook.AppointmentItem oAppt = (Outlook.AppointmentItem)calendarItems.GetFirst();
                            int i = calendarItems.Count;

                            while (oAppt != null)
                            {
                                try
                                {
                                    int iStart = DateTime.Compare(oAppt.Start, Convert.ToDateTime(evtObj.eventStartTime));
                                    int iEnd = DateTime.Compare(oAppt.End, Convert.ToDateTime(evtObj.eventEndTime));

                                    string sub = oAppt.Subject;
                                    string postData = "action=update_event&eventId=" + oAppt.BillingInformation + "&userId=" + postEmailDetails.loginUniqueKey + "&eventStartTime=" + oAppt.Start + "&eventEndTime=" + oAppt.End;

                                    if (oAppt.BillingInformation == evtObj.eventId && iStart == 0 && iEnd == 0 && evtObj.eventStatus == "trash")
                                    {
                                        oAppt.Delete();
                                        oAppt = (Outlook.AppointmentItem)calendarItems.GetNext();                                        
                                    }
                                    else if (oAppt.BillingInformation == evtObj.eventId && iStart == 0 && iEnd == 0 && evtObj.eventStatus == "deleted")
                                    {
                                        oAppt.Delete();
                                        oAppt = (Outlook.AppointmentItem)calendarItems.GetNext();                                       
                                    }
                                    else
                                        oAppt = (Outlook.AppointmentItem)calendarItems.GetNext();
                                }catch(Exception err)
                                {
                                    break;
                                }
                            }
                        }                       
                    }
                }

                if (bStatuss)
                {
                   
                    //update the new id
                    var sampe = (Newtonsoft.Json.Linq.JArray)returnValue.response;
                    EventDetails[] eventList = sampe.ToObject<EventDetails[]>();

                    if (postEmailDetails == null)
                    {
                        postEmailDetails = new PostEmailDetails();
                    }
                    eventDetailsList.Clear();

                   

                    foreach (EventDetails evtObj in eventList)
                    {
                        Outlook.NameSpace oNS = outlookApp.GetNamespace("mapi");

                        // Get the Calendar folder.
                        Outlook.MAPIFolder oCalendar = oNS.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderCalendar);

                        // Get the Items (Appointments) collection from the Calendar folder.
                        Outlook.Items oItems = oCalendar.Items;

                        Outlook.AppointmentItem oAppt = (Outlook.AppointmentItem)oItems.GetFirst();
                        while (oAppt != null)
                        {
                            try
                            {
                                int iStart = DateTime.Compare(oAppt.Start, Convert.ToDateTime(evtObj.eventStartTime));
                                int iEnd = DateTime.Compare(oAppt.End, Convert.ToDateTime(evtObj.eventEndTime));

                                if (oAppt.BillingInformation == evtObj.eventId &&
                                    iStart == 0 &&
                                    iEnd == 0)
                                {
                                    evtObj.isExists = true;
                                    break;
                                }
                            }
                            catch (Exception err)
                            {
                                break;
                            }

                            oAppt = (Outlook.AppointmentItem)oItems.GetNext();
                        }

                        postEmailDetails.eventListFromServer.Add(evtObj);
                        oNS.Logoff();
                    }

                    foreach (EventDetails evtDetails in postEmailDetails.eventListFromServer)
                    {
                        if (!evtDetails.isExists )
                        {
                            Outlook.AppointmentItem oAppointment = (Outlook.AppointmentItem)outlookApp.CreateItem(Outlook.OlItemType.olAppointmentItem); // creates a new appointment
                            oAppointment.Subject = evtDetails.eventTitle ; // set the subject
                            oAppointment.Body = evtDetails.eventDetails; // set the body
                            oAppointment.Location = ConvertListToString(evtDetails.locationLst);  // set the location
                            oAppointment.Start = Convert.ToDateTime(evtDetails.eventStartTime); // Set the start date 
                            oAppointment.End = Convert.ToDateTime(evtDetails.eventEndTime); // End date 
                            oAppointment.ReminderSet = false; // Set the reminder
                            //oAppointment.ReminderMinutesBeforeStart = 15; // reminder time
                            oAppointment.Importance = Outlook.OlImportance.olImportanceHigh; // appointment importance
                            oAppointment.BusyStatus = Outlook.OlBusyStatus.olBusy;
                            oAppointment.Categories = ConvertListToString(evtDetails.categoryLst);
                            oAppointment.BillingInformation = evtDetails.eventId;  
                            oAppointment.Save();

                            // response event
                            string postData = "action=update_event&eventId=" + evtDetails.eventId + "&userId=" + postEmailDetails.loginUniqueKey + "&eventStartTime=" + evtDetails.eventStartTime + "&eventEndTime=" + evtDetails.eventEndTime;

                            GenerateServiceCall(postData, "", "POST");
                        }
                    }

                    if (!isTimercall && postEmailDetails.eventListFromServer.Count > 0)
                    {
                            MessageBoxResult dialogResult = System.Windows.MessageBox.Show("Successfully Updated!!!", "IUIH", MessageBoxButton.OK, MessageBoxImage.Information);                       
                    }                   
                }
                else
                {
                    string responseStatus = (((Newtonsoft.Json.Linq.JValue)returnValue.response).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnValue.response).Value);

                    if (!isTimercall && !string.IsNullOrEmpty(responseStatus))
                    {                       
                        System.Windows.MessageBox.Show(responseStatus, "IUIH", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }

                if (isTimercall)
                    GetRsvpTotaluser();
            }
            catch (Exception err)
            {
                return eventDetailsList;
            }

            return eventDetailsList;
        }

        private void GetRsvpTotaluser()
        {
            try
            {
                Outlook.Application outlookApp = new Outlook.Application(); // creates new outlook app

                Outlook.NameSpace oNS = outlookApp.GetNamespace("mapi");

                // Get the Calendar folder.
                Outlook.MAPIFolder oCalendar = oNS.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderCalendar);

                // Get the Items (Appointments) collection from the Calendar folder.
                Outlook.Items oItems = oCalendar.Items;

                Outlook.AppointmentItem oAppt = (Outlook.AppointmentItem)oItems.GetFirst();
                while (oAppt != null)
                {
                    try
                    {
                        //check the end date is ellasped 
                        int eventClosed = DateTime.Compare(oAppt.End, DateTime.Now);

                        if (eventClosed > 0 && oAppt.BillingInformation != "")
                        {
                            string strDate = oAppt.Start.ToString("dd/M/yyyy HH:mm", CultureInfo.InvariantCulture);
                            string str = "action=get_event_rsvp_details&eventId=" + oAppt.BillingInformation + "&eventStartTime=" + oAppt.Start.ToString("dd/M/yyyy HH:mm", CultureInfo.InvariantCulture) + "&eventEndTime=" + oAppt.End.ToString("dd/M/yyyy HH:mm", CultureInfo.InvariantCulture);
                            //proceed only when the end date is live
                            dynamic returnValue = GenerateServiceCall("action=get_event_rsvp_details&eventId=" + oAppt.BillingInformation + "&eventStartTime=" + oAppt.Start.ToString("dd/M/yyyy HH:mm", CultureInfo.InvariantCulture) + "&eventEndTime=" + oAppt.End.ToString("dd/M/yyyy HH:mm", CultureInfo.InvariantCulture), "", "POST");

                            if (returnValue == null)
                                return;

                            bool bStatuss = (((Newtonsoft.Json.Linq.JValue)returnValue.status).Value == null) ? false : Convert.ToBoolean(((Newtonsoft.Json.Linq.JValue)returnValue.status).Value);

                            if (bStatuss)
                            {
                                // get the total user response
                                int rsvpTotaluser = (((Newtonsoft.Json.Linq.JValue)returnValue.response.rsvpTotalUser).Value == null) ? 0 : Convert.ToInt32(((Newtonsoft.Json.Linq.JValue)returnValue.response.rsvpTotalUser).Value);
                                if (rsvpTotaluser > 0)
                                {
                                    //update the subject
                                    oAppt.Subject = oAppt.Subject + " (Accepted user: " + rsvpTotaluser + " )";                                  
                                }
                            }
                        }
                    }
                    catch (Exception err)
                    {
                        break;
                    }

                    oAppt = (Outlook.AppointmentItem)oItems.GetNext();
                }
                oNS.Logoff();
            }
            catch (Exception err)
            {
                return;
            }

            return;
        }

        private string ConvertListToString(List<string> collectionDet)
        {
            string sReturn = "";

           foreach(string str in collectionDet)
            {
                sReturn = (sReturn == "") ? str : sReturn +"," + str;
            }

            return sReturn;
        }

        public void btnRefreshEventOnClick(Office.IRibbonControl control)
        {
            try
            {              

                postEmailDetails = LoadRegValues(false);

                if (postEmailDetails.isLoggedIn)
                {
                    MessageBoxResult dialogResult = System.Windows.MessageBox.Show("Do you want to sync calender events?", "IUIH", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (dialogResult == MessageBoxResult.Yes)
                    {
                        SyncCalenderEvents(false);
                    }
                }
            }
            catch (Exception err)
            {
            }
        }      


        public bool GetLogoutEnabled(Office.IRibbonControl control)
        {
            bool breturn = false;

            RegistryKey key = Registry.CurrentUser.OpenSubKey(registryPath, true);

            if (key == null)
                key = Registry.CurrentUser.CreateSubKey(registryPath, RegistryKeyPermissionCheck.Default);

            if (!string.IsNullOrEmpty(Convert.ToString(key.GetValue("UserName"))) && !string.IsNullOrEmpty(Convert.ToString(key.GetValue("Password"))))
            {
                breturn = true;

            }
            else
                breturn = false;

            return breturn;
        }

        public bool GetLoginEnabled(Office.IRibbonControl control)
        {
            bool breturn = false;

            RegistryKey key = Registry.CurrentUser.OpenSubKey(registryPath, true);

            if (key == null)
                key = Registry.CurrentUser.CreateSubKey(registryPath, RegistryKeyPermissionCheck.Default);

            if (!string.IsNullOrEmpty(Convert.ToString(key.GetValue("UserName"))) && !string.IsNullOrEmpty(Convert.ToString(key.GetValue("Password"))))
            {
                breturn = false;

            }
            else
                breturn = true;

            return breturn;
        }

        public void btnLogin(Office.IRibbonControl control)
        {
            try
            {
                //this.ribbon.Invalidate();
                LoadRegValues(true);
            }
            catch (Exception err)
            {
            }
        }


        public void btnLogout(Office.IRibbonControl control)
        {
            PostEmailDetails postEmailDetails = LoadRegValues(false);
            try
            {
                if (postEmailDetails != null && postEmailDetails.isLoggedIn)
                {
                    try
                    {
                        MessageBoxResult dialogResult = System.Windows.MessageBox.Show("Are you sure you want to logout?", "IUIH", MessageBoxButton.YesNo, MessageBoxImage.Question);
                        if (dialogResult == MessageBoxResult.Yes)
                        {
                            if (string.IsNullOrEmpty(postEmailDetails.loginUniqueKey))
                                return;
                        
                                //save the vales entred in UI to registry
                                RegistryKey key = Registry.CurrentUser.OpenSubKey(postEmailDetails.registryPath, true);

                                if (key == null)
                                    key = Registry.CurrentUser.CreateSubKey(postEmailDetails.registryPath);
                             
                            key.SetValue("LoginUserName", "");
                            key.SetValue("Password", "");
                            key.SetValue("UserName", "");
                            key.SetValue("LoginUniqueKey", "");
                            
                            key.Close();                           
                            GC.Collect();

                            MessageBoxResult result = System.Windows.MessageBox.Show("You are logged out successfully.", "IUIH", MessageBoxButton.OK, MessageBoxImage.Information);
                                                       
                            this.ribbon.Invalidate();                           
                        }
                    }
                    catch (Exception err)
                    {
                        System.Windows.MessageBox.Show(err.Message, "IUIH", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }               
            }
            catch (Exception err)
            {
            }
        }

        public Bitmap imageSuper_GetImage(Office.IRibbonControl control)
        {
            return IUIHResource.logo;
        }

        private PostEmailDetails LoadRegValues(bool isFromLogin)
        {
            PostEmailDetails postEmailDetails = new PostEmailDetails();
            try
            {
                postEmailDetails.serviceURL = ServiceURL;
                postEmailDetails.registryPath = registryPath;
                postEmailDetails.authPassword = authPassword;
                postEmailDetails.authUserName = authUserName;
          
                //Open and read the registry values- create if not found
                RegistryKey key = Registry.CurrentUser.OpenSubKey(registryPath, true);

                if (key == null)
                    key = Registry.CurrentUser.CreateSubKey(registryPath, RegistryKeyPermissionCheck.Default);

                if (key.GetValue("UserName") == null)
                    key.SetValue("UserName", "");

                if (key.GetValue("Password") == null)
                    key.SetValue("Password", "");

                if (key.GetValue("LoginUniqueKey") == null)
                    key.SetValue("LoginUniqueKey", "");

                if (!isFromLogin)
                {
                    if (string.IsNullOrEmpty(Convert.ToString(key.GetValue("UserName"))) || string.IsNullOrEmpty(Convert.ToString(key.GetValue("Password"))))
                    {
                        postEmailDetails.isLoggedIn = false;
                        // invoke the settings page, if not configured for addins.
                        key.Close();

                        MessageBoxResult dialogResult = System.Windows.MessageBox.Show("You are not logged in. Do you want to login?", "IUIH", MessageBoxButton.YesNo, MessageBoxImage.Information);
                        if (dialogResult == MessageBoxResult.Yes)
                        {
                            LoginWPF fromLogin = new LoginWPF(postEmailDetails);
                            fromLogin.ShowDialog();                           
                        }

                        return postEmailDetails;
                    }
                    else
                        postEmailDetails.isLoggedIn = true;

                    postEmailDetails.userName = key.GetValue("UserName").ToString().Trim();
                    postEmailDetails.loginUniqueKey = key.GetValue("LoginUniqueKey").ToString().Trim();
                }
                else
                {
                    LoginWPF fromLogin = new LoginWPF(postEmailDetails);
                    fromLogin.ShowDialog();                    
                }

                key.Close();           
            }
            catch (Exception err)
            {
                // MessageBox.Show("Error:" + logStages + ", Startup: " + err.Message);
            }

            return postEmailDetails;
        }

      
      #region IRibbonExtensibility Members

        public string GetCustomUI(string ribbonID)
        {
            if (ribbonID == "Microsoft.Outlook.Explorer")
                return GetResourceText("IUIH.Ribbon.xml");

            return null; // if problems here, try return string.Empty
        }

        #endregion IRibbonExtensibility Members

        #region Ribbon Callbacks

        //Create callback methods here. For more information about adding callback methods, visit http://go.microsoft.com/fwlink/?LinkID=271226

        public void Ribbon_Load(Office.IRibbonUI ribbonUI)
        {
            this.ribbon = ribbonUI;
            this.ribbon.Invalidate();            
        }

        #endregion Ribbon Callbacks

        #region Helpers

        private static string GetResourceText(string resourceName)
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            string[] resourceNames = asm.GetManifestResourceNames();
            for (int i = 0; i < resourceNames.Length; ++i)
            {
                if (string.Compare(resourceName, resourceNames[i], StringComparison.OrdinalIgnoreCase) == 0)
                {
                    using (StreamReader resourceReader = new StreamReader(asm.GetManifestResourceStream(resourceNames[i])))
                    {
                        if (resourceReader != null)
                        {
                            return resourceReader.ReadToEnd();
                        }
                    }
                }
            }
            return null;
        }

     
        #endregion Helpers
    }
}