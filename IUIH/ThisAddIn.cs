﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace IUIH
{
    public partial class ThisAddIn
    {        
        //private Outlook.NameSpace outlookNameSpace;
        private Outlook.MAPIFolder inbox;
        private Outlook.Items items;    

        private string registryPath = ConfigurationManager.AppSettings["RegistryPath"].ToString();
        private string ServiceURL = ConfigurationManager.AppSettings["ServiceURL"].ToString();            

        #region Outlook addin Startup event

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            try
            {
                //get the outlook items 
                inbox = this.Application.ActiveExplorer().Session.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderInbox);
                items = inbox.Items;              
            }
            catch (Exception err)
            {
            }
        }
              

        protected override Microsoft.Office.Core.IRibbonExtensibility CreateRibbonExtensibilityObject()
        {
            return new Ribbon();
        }

        #endregion Outlook addin Startup event

       

        #region outlook addins shutdown event

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
            this.Startup -= new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown -= new System.EventHandler(ThisAddIn_Shutdown);
        }

        #endregion outlook addins shutdown event

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }

        #endregion VSTO generated code
    }

    //class to maintain the configuration values
    public class PostEmailDetails
    {
        public string loginUniqueKey { get; set; }
        public string userName { get; set; }
        public string serviceURL { get; set; }
        public string registryPath { get; set; }

        public string authUserName { get; set; }
        public string authPassword { get; set; }
        public List<EventDetails> eventListFromServer = new List<EventDetails>();
        public List<EventDetails> eventListFromCalander = new List<EventDetails>();
        public bool isLoggedIn { get; set; }
        public string installationPath { get; set; }
        public string fileUploadFolder { get; set; }
       
    }
}