﻿using MahApps.Metro.Controls;
using Microsoft.Win32;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace IUIH
{
    /// <summary>
    /// Interaction logic for MainFormWPF.xaml
    /// </summary>
    public partial class MainFormWPF : MetroWindow
    {
        private PostEmailDetails fromPostEmailContent = new PostEmailDetails();
        private bool copyFromMail = false;
        private bool isCopyClientAddress = false;
        private readonly BackgroundWorker _bw = new BackgroundWorker();

       
        public MainFormWPF(PostEmailDetails passPostEmailContent, bool isCopyMail)
        {
            try
            {
                InitializeComponent();
                pbProcess.Value = 0;
                grpBoxLeadClient.Visibility = Visibility.Visible;
                grpBoxContact.Visibility = Visibility.Hidden;

                fromPostEmailContent = passPostEmailContent;
                copyFromMail = isCopyMail;

                if (fromPostEmailContent.contextSelection == "AddClientContactExist")
                    isCopyClientAddress = true;
                else
                    isCopyClientAddress = false;


                //_bw.DoWork += _bw_DoWork;
                //_bw.RunWorkerCompleted += BwRunWorkerCompleted;
                
                _bw.DoWork += Save;
                _bw.ProgressChanged += worker_ProgressChanged;               
                _bw.WorkerReportsProgress = true;
               
                pbProcess.Minimum = 1;
                pbProcess.Maximum = 100;

                //if (login != null)
                //{
                //    login.Close();
                //}
               

                //set default index for combobox - Company Name
                ClearCompanyCombo();


                ClearValues(fromPostEmailContent);

                BindMasterValues();

            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "IUIH", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
            

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            double percent = (e.ProgressPercentage * 100) / 50;

            pbProcess.Value = Math.Round(percent, 0);
        }


        private void ProcessShow()
        {
            //pbProcess.sh();
            //for (int i = 0; i < 100; i++)
            //{
            //    (sender as BackgroundWorker).ReportProgress(i);
            //    Thread.Sleep(100);
            //}
            //  pbProcess.Style = ProgressBarStyle.Marquee;
            //   pbProcess.MarqueeAnimationSpeed = 30;
            buttonSave.IsEnabled = false;
            tabMain.IsEnabled = false;
            //tabSelector.Enabled = false;
        }

        private void ProcessHide()
        {
            //pbProcess.Hide();
            buttonSave.IsEnabled = true;
            tabMain.IsEnabled = true;
            //tabSelector.Enabled = true;
            //pbProcess.Style = ProgressBarStyle.Continuous;
            //pbProcess.MarqueeAnimationSpeed = 0;
            this.Close();
        }

        private void BindMasterValues()
        {
            //Get company Name from service
            dynamic returnValue = GetServiceCall("getcompany", "", "GET");
            cmbBoxRelatedTo.IsEnabled = true;
            if (returnValue == null)
                return;

            bool bStatuss = (((Newtonsoft.Json.Linq.JValue)returnValue.success).Value == null) ? false : Convert.ToBoolean(((Newtonsoft.Json.Linq.JValue)returnValue.success).Value);

            if (bStatuss)
            {
                var sampe = (Newtonsoft.Json.Linq.JArray)returnValue.result;

                cmbCompanyName.Items.Clear();
                //set default index for combobox
                ComboboxItem itemnew = new ComboboxItem();
                itemnew.Text = "-- Company Name --";
                itemnew.Value = "0";
                cmbCompanyName.Items.Add(itemnew);
                cmbCompanyName.SelectedIndex = 0;

                CompanyNameCollection[] companyName = sampe.ToObject<CompanyNameCollection[]>();

                foreach (CompanyNameCollection cmpname in companyName.OrderBy(x => x.company))
                {
                    ComboboxItem item = new ComboboxItem();
                    item.Text = cmpname.company;
                    item.Value = cmpname.id;
                    cmbCompanyName.Items.Add(item);
                    cmbBoxRelatedTo.Items.Add(item);
                }


                if (fromPostEmailContent.listClientDetailsCommon.Count > 0 && !string.IsNullOrEmpty(fromPostEmailContent.mailFrom))
                {
                    ClientList clientList = fromPostEmailContent.listClientDetailsCommon.Where(x => x.ClientEmail != null && x.ClientEmail.Trim().ToUpper() == fromPostEmailContent.mailFrom.Trim().ToUpper()).Select(x => x).FirstOrDefault();

                    if (clientList != null)
                    {
                        fromPostEmailContent.clientIdForSelection = clientList.id;
                    }
                }

                if (!string.IsNullOrEmpty(fromPostEmailContent.clientIdForSelection))
                {
                    object selectItm = cmbCompanyName.Items.Cast<ComboboxItem>().Where(x => x.Value.ToString() == fromPostEmailContent.clientIdForSelection).FirstOrDefault();

                    if (selectItm != null)
                    {
                        //cmbCompanyName.SelectionChanged += cmbCompanyName_SelectionChanged;
                        cmbCompanyName.SelectedItem = selectItm;

                    }
                }
            }
            else
            {
                string errorMessage = (((Newtonsoft.Json.Linq.JValue)returnValue.error.message).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnValue.error.message).Value);
                MessageBox.Show(errorMessage, "IUIH", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            //get status from service
            dynamic returnValueStatus = GetServiceCall("getleadstatus", "", "GET");
            bool bStatusVal = (((Newtonsoft.Json.Linq.JValue)returnValueStatus.success).Value == null) ? false : Convert.ToBoolean(((Newtonsoft.Json.Linq.JValue)returnValueStatus.success).Value);

            if (bStatusVal)
            {
                var sampe = (Newtonsoft.Json.Linq.JArray)returnValueStatus.result;

                StatusCollection[] statusColl = sampe.ToObject<StatusCollection[]>();

                fromPostEmailContent.leadStatus.Clear();
                foreach (StatusCollection statusDetails in statusColl.OrderBy(x => x.name))
                {
                    ComboboxItem item = new ComboboxItem();
                    item.Text = statusDetails.name;
                    item.Value = statusDetails.id;
                    fromPostEmailContent.leadStatus.Add(item);
                    //cmboxStatus.Items.Add(item);
                }
            }
            else
            {
                string errorMessage = (((Newtonsoft.Json.Linq.JValue)returnValueStatus.error.message).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnValueStatus.error.message).Value);
                MessageBox.Show(errorMessage, "IUIH", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            //get client status from service
            dynamic returngetclientstatus = GetServiceCall("getclientstatus", "", "GET");
            bool bStatusclientstatus = (((Newtonsoft.Json.Linq.JValue)returngetclientstatus.success).Value == null) ? false : Convert.ToBoolean(((Newtonsoft.Json.Linq.JValue)returngetclientstatus.success).Value);

            if (bStatusclientstatus)
            {
                var sampe = (Newtonsoft.Json.Linq.JArray)returngetclientstatus.result;

                StatusCollection[] statusColl = sampe.ToObject<StatusCollection[]>();
                fromPostEmailContent.clientStatus.Clear();
                foreach (StatusCollection statusDetails in statusColl.OrderBy(x => x.name))
                {
                    ComboboxItem item = new ComboboxItem();
                    item.Text = statusDetails.name;
                    item.Value = statusDetails.id;
                    fromPostEmailContent.clientStatus.Add(item);
                }
            }
            else
            {
                string errorMessage = (((Newtonsoft.Json.Linq.JValue)returngetclientstatus.error.message).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returngetclientstatus.error.message).Value);
                MessageBox.Show(errorMessage, "IUIH", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private dynamic GetServiceCall(string serviceMethod, string serviceParameter, string requestMethod)
        {
            dynamic jsonReturn = null;

            try
            {
                HttpWebRequest request = null;
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                if (!string.IsNullOrEmpty(serviceParameter) && requestMethod.ToUpper() == "GET")
                    request = (HttpWebRequest)WebRequest.Create(fromPostEmailContent.serviceURL + serviceMethod + "/" + serviceParameter);
                else
                    request = (HttpWebRequest)WebRequest.Create(fromPostEmailContent.serviceURL + serviceMethod + "/");

                if (requestMethod.ToUpper() == "POST")
                {
                    var data = Encoding.UTF8.GetBytes(serviceParameter);

                    request.Method = "POST";
                    request.ContentType = "application/x-www-form-urlencoded";
                    request.ContentLength = data.Length;
                    request.KeepAlive = true;
                    request.Accept = "*/*";
                    //request.ClientCertificates = lcCertificates;

                    using (var stream = request.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                        stream.Close();//new
                    }
                }

                var response = (HttpWebResponse)request.GetResponse();

                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                string stringResponseString = Convert.ToString(responseString);

                dynamic returnResponse = stringResponseString.Contains("<div") ? null : JsonConvert.DeserializeObject(responseString);

                jsonReturn = returnResponse;

                return jsonReturn;
            }
            catch (Exception err)
            {
                if(err.Message != null)
                    MessageBox.Show(err.Message, "IUIH", MessageBoxButton.OK, MessageBoxImage.Error);
                else if (err.InnerException != null)
                    MessageBox.Show(err.InnerException.Message, "IUIH", MessageBoxButton.OK, MessageBoxImage.Error);
                else 
                    MessageBox.Show("Exception occured from server. Kindly contact admin", "IUIH", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return jsonReturn;
        }
              

        private void ContactDetailBind(ComboboxItem cmb)
        {
            txtBoxContactFirstName.Text = string.Empty;
            txtBoxContactLastName.Text = string.Empty;
            txtBoxContactEmail.Text = string.Empty;
            txtBoxContactPhone.Text = string.Empty;
            txtBoxContactSkype.Text = string.Empty;

            var postData = cmb != null ? "id=" + cmb.Value.ToString() : "id=0";
            dynamic returnValue = GetServiceCall("getcontactdetails", postData, "POST");

            if (returnValue != null)
            {
                bool bStatuss = (((Newtonsoft.Json.Linq.JValue)returnValue.success).Value == null) ? false : Convert.ToBoolean(((Newtonsoft.Json.Linq.JValue)returnValue.success).Value);

                if (bStatuss)
                {
                    chBNewContact.IsChecked = false;
                    txtBoxContactFirstName.Text = (((Newtonsoft.Json.Linq.JValue)returnValue.result.first_name).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnValue.result.first_name).Value);
                    txtBoxContactLastName.Text = (((Newtonsoft.Json.Linq.JValue)returnValue.result.last_name).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnValue.result.last_name).Value);

                    txtBoxContactEmail.Text = (((Newtonsoft.Json.Linq.JValue)returnValue.result.email).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnValue.result.email).Value);
                    txtBoxContactPhone.Text = (((Newtonsoft.Json.Linq.JValue)returnValue.result.phone).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnValue.result.phone).Value);
                    txtBoxContactSkype.Text = (((Newtonsoft.Json.Linq.JValue)returnValue.result.skype).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnValue.result.skype).Value);
                }
                else
                {
                    string errorMessage = (((Newtonsoft.Json.Linq.JValue)returnValue.error.message).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnValue.error.message).Value);
                    MessageBox.Show(errorMessage, "IUIH", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void BwRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
        }


        private void cmbCompanyName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
               
                rdBtnPrivate.IsChecked = false;
                rdBtnDevTeam.IsChecked = false;
                rdBtnMyFranchise.IsChecked = false;
                rdBtnFranchisee.IsChecked = false;
                rdBtnTSUSA.IsChecked = false;

                ComboBox cmbCompanyNm = (ComboBox)sender;

                if (cmbCompanyNm.SelectedIndex == -1)
                    return;

                if (cmbCompanyNm.SelectedIndex != 0)
                {
                    ComboboxItem cmb = (ComboboxItem)cmbCompanyNm.SelectedItem;
                    CompanyDetailBind(cmb);
                }
                else
                {
                    txtboxCompanyName.Text = string.Empty;
                    txtBoxLastName.Text = string.Empty;
                    txtboxNotes.Text = string.Empty;
                    textBoxContents.Text = string.Empty;
                    txtboxPhone.Text = string.Empty;
                    lblSalesUnit.Text = fromPostEmailContent.salesUnit;
                    txtboxSkype.Text = string.Empty;
                    txtboxEmail.Text = string.Empty;
                    lblCompnayClinetName.Text = string.Empty;
                    dateTimeDueDate.Value = DateTime.Now;
                    dateTimeRemainder.Value = DateTime.Now;
                    txtBoxDescription.Text = string.Empty;
                    rdButtonClient.IsChecked = false;
                    rdButtonLead.IsChecked = false;
                    txtBoxContactEmail.Text = "";
                    txtBoxContactFirstName.Text = "";
                    txtBoxContactLastName.Text = "";
                    txtBoxContactPhone.Text = "";
                    txtBoxContactSkype.Text = "";

                    ClearTasksRemainderCombo();
                    ClearContactsCombo();
                    ClearStatusCombo();

                    cmbBoxRelatedTo.SelectedIndex = 0;
                    cmbBoxRelatedTo.IsEnabled = true;
                    cmbboxTasksRemainder.IsEnabled = true;
                }

                SetSharingGroups();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "IUIH", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearValues(PostEmailDetails passPostEmailContent, bool isAfterSave = false)
        {
            ////clear all fields
            rchboxAttachment.Visibility = Visibility.Collapsed;
            rchboxAttachment.Text = string.Empty;
            txtboxCompanyName.Text = string.Empty;
            textBoxFirstName.Text = string.Empty;
            txtBoxLastName.Text = string.Empty;
            txtboxNotes.Text = string.Empty;
            textBoxContents.Text = string.Empty;
            txtboxPhone.Text = string.Empty;
            lblSalesUnit.Text = fromPostEmailContent.salesUnit;
            txtboxSkype.Text = string.Empty;
            txtboxEmail.Text = string.Empty;
            checkBoxNewTask.IsChecked = false;
            chBNewContact.IsChecked = false;

            if (!isAfterSave)
                cmbCompanyName.Items.Clear();

            lblCompnayClinetName.Text = string.Empty;
            textBoxWebsite.Text = string.Empty;
            txtBoxTitle.Text = string.Empty;
            textBoxContents.Text = string.Empty;

            dateTimeDueDate.Value = DateTime.Now;
            dateTimeRemainder.Value = DateTime.Now;
            lblSalesUnit.Text = passPostEmailContent.salesUnit;

            txtBoxDescription.Text = string.Empty;
            rdButtonClient.IsChecked = false;
            rdButtonLead.IsChecked = false;

            rdButtonLead.IsEnabled = true;
            rdButtonClient.IsEnabled = true;

            txtBoxContactEmail.Text = "";
            txtBoxContactFirstName.Text = "";
            txtBoxContactLastName.Text = "";
            txtBoxContactPhone.Text = "";
            txtBoxContactSkype.Text = "";

            cmbboxTasksRemainder.IsEnabled = true;

            //set default index for combobox - Related To
            ClearRelateCombo();
            ClearStatusCombo();
            ClearTasksRemainderCombo();
            ClearContactsCombo();
            cmbBoxTaskType.SelectedIndex = 0;
        }

        private void FillStatusCombo(List<ComboboxItem> statuses)
        {
            foreach (ComboboxItem status in statuses)
            {
                cmboxStatus.Items.Add(status);
            }
        }

        private void FillStatusComboFromContext(List<ComboboxItem> statuses, string selection)
        {
            foreach (ComboboxItem status in statuses)
            {
                if (status.Text.ToUpper() == selection.ToUpper())
                {
                    cmboxStatus.Items.Add(status);
                    break;
                }
            }

            //int index = cmboxStatus.FindString(selection);
            int index = cmboxStatus.Items.IndexOf(selection);
            cmboxStatus.SelectedIndex = index;
        }

        private void ClearCompanyCombo()
        {
            ComboboxItem itemnew = new ComboboxItem();
            itemnew.Text = "-- Company Name --";
            itemnew.Value = "0";
            cmbCompanyName.Items.Add(itemnew);
            cmbCompanyName.SelectedIndex = 0;
        }

        private void ClearStatusCombo()
        {
            cmboxStatus.Items.Clear();
            ComboboxItem itemnewStatus = new ComboboxItem();
            itemnewStatus.Text = "-- Status --";
            itemnewStatus.Value = "0";
            cmboxStatus.Items.Add(itemnewStatus);
            cmboxStatus.SelectedIndex = 0;
        }

        private void ClearRelateCombo()
        {
            cmbBoxRelatedTo.Items.Clear();
            ComboboxItem itemRelatedTo = new ComboboxItem();
            itemRelatedTo.Text = "-- Related To --";
            itemRelatedTo.Value = "0";
            cmbBoxRelatedTo.Items.Add(itemRelatedTo);
            cmbBoxRelatedTo.SelectedIndex = 0;
        }

        private void ClearTasksRemainderCombo()
        {
            cmbboxTasksRemainder.Items.Clear();
            ComboboxItem itemnewReaminder = new ComboboxItem();
            itemnewReaminder.Text = "-- Task/Remainder --";
            itemnewReaminder.Value = "0";
            cmbboxTasksRemainder.Items.Add(itemnewReaminder);
            cmbboxTasksRemainder.SelectedIndex = 0;
        }

        private void ClearContactsCombo()
        {
            cmbBoxListOfContacts.Items.Clear();
            ComboboxItem itemnewContact = new ComboboxItem();
            itemnewContact.Text = "-- Contacts --";
            itemnewContact.Value = "0";
            cmbBoxListOfContacts.Items.Add(itemnewContact);
            cmbBoxListOfContacts.SelectedIndex = 0;
        }

        private void SetSharingGroups()
        {
            string selectedSalesUnit = (fromPostEmailContent.salesUnitId == "") ? "" : fromPostEmailContent.salesUnitId.Split('_')[1];

            if (fromPostEmailContent.role == "2")
                rdBtnMyFranchise.IsEnabled = true;

            if ((fromPostEmailContent.role == "1" || fromPostEmailContent.role == "3") && fromPostEmailContent.role != "2" && selectedSalesUnit != "1" && !fromPostEmailContent.isAdmin)
                rdBtnFranchisee.IsEnabled = true;
            else if (selectedSalesUnit != "1" && fromPostEmailContent.isAdmin && fromPostEmailContent.role != "2")
                rdBtnFranchisee.IsEnabled = true;

            if ((fromPostEmailContent.role == "2" || fromPostEmailContent.role == "1") && selectedSalesUnit != "1" && !fromPostEmailContent.isAdmin)
                rdBtnTSUSA.IsEnabled = true;
        }

        private void cmbCompanyName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                // e.SuppressKeyPress = true;
                foreach (ComboboxItem cmb in cmbCompanyName.Items)
                {
                    if (cmb.Text.ToLower().TrimStart().TrimEnd() == cmbCompanyName.Text.ToLower().TrimStart().TrimEnd())
                    {
                        CompanyDetailBind(cmb);
                        break;
                    }
                }

            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //this.AutoSize = true;
            //this.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            ////this.Text = "URL Opener";
            ////   flowPanel = new FlowLayoutPanel();
            ////  flowPanel.AutoSize = false;
            ////  flowPanel.AutoSizeMode = AutoSizeMode;
            ////  this.Controls.Add(flowPanel);

            rdBtnPrivate.IsEnabled = true;
            rdBtnDevTeam.IsEnabled = true;
            rdBtnMyFranchise.IsEnabled = false;
            rdBtnFranchisee.IsEnabled = false;
            rdBtnTSUSA.IsEnabled = false;

            SetSharingGroups();

            if (copyFromMail)
            {
                if (isCopyClientAddress)
                {
                    chBNewContact.IsChecked = true;
                    txtBoxContactEmail.Text = "";
                    txtBoxContactFirstName.Text = "";
                    txtBoxContactLastName.Text = "";
                    txtBoxContactPhone.Text = "";
                    txtBoxContactSkype.Text = "";

                    txtBoxContactEmail.Text = fromPostEmailContent.clientContactEmail;
                    txtBoxContactFirstName.Text = fromPostEmailContent.clientContactFirstName;
                    txtBoxContactLastName.Text = fromPostEmailContent.clientContactLastName;
                    tabMain.SelectedIndex = 0;

                    rdButtonClient.IsEnabled = true;
                    rdButtonClient.IsChecked = false;

                    rdButtonLead.IsEnabled = true;
                    rdButtonLead.IsChecked = false;

                    chkboxContact.IsChecked = true;
                    grpBoxLeadClient.Visibility = Visibility.Hidden;
                    grpBoxContact.Visibility = Visibility.Visible;
                }
                else
                {

                    grpBoxLeadClient.Visibility = Visibility.Visible;
                    grpBoxContact.Visibility = Visibility.Hidden;

                    //tabSelector.BaseTabControl.SelectedIndex = 0;
                    textBoxContents.Focus();
                    //// mail content copied by right click
                    cmboxStatus.SelectedItem = cmboxStatus.Items.Cast<ComboboxItem>().Where(x => x.Text.ToString().Trim() == fromPostEmailContent.salesUnit.Trim()).FirstOrDefault();

                    if (!string.IsNullOrEmpty(fromPostEmailContent.mailBody))
                        textBoxContents.Text = fromPostEmailContent.mailBody;
                    else
                        textBoxContents.Text = "";

                    if (!string.IsNullOrEmpty(fromPostEmailContent.contextSelection))
                    {
                        if (fromPostEmailContent.contextSelection == "NewLead")
                            SetLeadORClientStatus("LEAD");
                        else if (fromPostEmailContent.contextSelection == "NewClient")
                            SetLeadORClientStatus("CLIENT");
                    }

                    if (!string.IsNullOrEmpty(fromPostEmailContent.senderName) && (fromPostEmailContent.contextSelection == "NewLead" || fromPostEmailContent.contextSelection == "NewClient"))
                    {
                        //if (fromPostEmailContent.contextSelection == "NewClient")
                        {
                            string recpName = fromPostEmailContent.senderName;
                            int index = recpName.IndexOf(' ');
                            string first = "";
                            string second = "";

                            if (index > 0)
                            {
                                first = recpName.Substring(0, index);
                                second = recpName.Substring(index + 1);
                            }
                            else if(! string.IsNullOrEmpty(recpName))
                            {
                                first = recpName;
                            }

                            textBoxFirstName.Text = first.Replace("'"," ").Trim();
                            txtBoxLastName.Text = second.Replace("'", " ").Trim();
                        }
                        //else
                        //    textBoxFirstName.Text = fromPostEmailContent.senderName;
                    }

                    if (!string.IsNullOrEmpty(fromPostEmailContent.mailFrom) && (fromPostEmailContent.contextSelection == "NewLead" || fromPostEmailContent.contextSelection == "NewClient"))
                    {
                        txtboxEmail.Text = fromPostEmailContent.mailFrom;
                    }

                    if (fromPostEmailContent.contextSelection == "NewLead" || fromPostEmailContent.contextSelection == "NewClient")
                        tabMain.SelectedIndex = 0;
                    else
                    {
                        tabMain.SelectedIndex = 2;
                    }

                }
            }
            else
                fromPostEmailContent.attachedFilePath.Clear();

            if (!string.IsNullOrEmpty(fromPostEmailContent.filesAttachedFromEmail))
            {
                //check the email is attached
                rchboxAttachment.Visibility = Visibility.Visible;
                rchboxAttachment.Text = fromPostEmailContent.filesAttachedFromEmail.Replace("|", " , ");
            }
        }

        private void SetLeadORClientStatus(string selection)
        {
            if (selection == "LEAD")
            {
                ClearStatusCombo();

                rdButtonLead.IsChecked = true;
                rdButtonClient.IsChecked = false;
                rdButtonClient.IsEnabled = false;

                FillStatusComboFromContext(fromPostEmailContent.leadStatus, "New Leads");
                cmboxStatus.IsEnabled = false;
            }
            else if (selection == "CLIENT")
            {
                ClearStatusCombo();

                rdButtonLead.IsChecked = false;
                rdButtonClient.IsChecked = true;
                rdButtonLead.IsEnabled = false;

                FillStatusComboFromContext(fromPostEmailContent.clientStatus, "New Client");
                cmboxStatus.IsEnabled = false;
            }
        }

        private void CompanyDetailBind(ComboboxItem cmb)
        {
            try
            {
                if (cmb != null)
                {
                    txtboxCompanyName.Text = cmb.Text;

                    var postData = "id=" + cmb.Value.ToString() + "&uid=" + fromPostEmailContent.loginUniqueKey;
                    cmbBoxRelatedTo.SelectedIndex = 0;

                    dynamic returnValue = GetServiceCall("getLeaddetails", postData, "POST");

                    if (returnValue != null)
                    {
                        bool bStatuss = (((Newtonsoft.Json.Linq.JValue)returnValue.success).Value == null) ? false : Convert.ToBoolean(((Newtonsoft.Json.Linq.JValue)returnValue.success).Value);

                        if (bStatuss)
                        {
                            string firstName = (Newtonsoft.Json.Linq.JValue)returnValue.result.firstname == null ? Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnValue.result.name).Value) : (((Newtonsoft.Json.Linq.JValue)returnValue.result.firstname).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnValue.result.firstname).Value);
                            string lastName = (Newtonsoft.Json.Linq.JValue)returnValue.result.lastname == null ? "" : (((Newtonsoft.Json.Linq.JValue)returnValue.result.lastname).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnValue.result.lastname).Value);
                            string status = (((Newtonsoft.Json.Linq.JValue)returnValue.result.status).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnValue.result.status).Value);
                            string email = (((Newtonsoft.Json.Linq.JValue)returnValue.result.email).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnValue.result.email).Value);

                            string phonenumber = (((Newtonsoft.Json.Linq.JValue)returnValue.result.phonenumber).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnValue.result.phonenumber).Value);
                            string skype = (((Newtonsoft.Json.Linq.JValue)returnValue.result.skype).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnValue.result.skype).Value);
                            string siteurl = (((Newtonsoft.Json.Linq.JValue)returnValue.result.siteurl).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnValue.result.siteurl).Value);

                            string clientRLead = (((Newtonsoft.Json.Linq.JValue)returnValue.result.istype).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnValue.result.istype).Value);
                            string leadSalesUnit = (((Newtonsoft.Json.Linq.JValue)returnValue.result.sales_unit).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnValue.result.sales_unit).Value);
                            string leadSalesUnitID = (((Newtonsoft.Json.Linq.JValue)returnValue.result.sales_unitId).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnValue.result.sales_unitId).Value);

                            var listofTasks = (Newtonsoft.Json.Linq.JArray)returnValue.result.tasklist;
                            var listofContacts = (Newtonsoft.Json.Linq.JArray)returnValue.result.contactlist;

                            string taskDueDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm");
                            string taskRemianderDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm");

                            dateTimeDueDate.Value = DateTime.Now;
                            dateTimeRemainder.Value = DateTime.Now;

                            if (!string.IsNullOrEmpty(taskDueDate))
                                dateTimeDueDate.Value = Convert.ToDateTime(taskDueDate);

                            if (!string.IsNullOrEmpty(taskRemianderDate))
                                dateTimeRemainder.Value = Convert.ToDateTime(taskRemianderDate);

                            ClearTasksRemainderCombo();

                            if (listofTasks != null)
                            {
                                StatusCollection[] taskColl = listofTasks.ToObject<StatusCollection[]>();

                                foreach (StatusCollection taskDetails in taskColl.OrderBy(x => x.name))
                                {
                                    ComboboxItem item = new ComboboxItem();
                                    item.Text = taskDetails.name;
                                    item.Value = taskDetails.id;
                                    cmbboxTasksRemainder.Items.Add(item);
                                }
                            }

                            ClearContactsCombo();
                            if (listofContacts != null)
                            {
                                StatusCollection[] taskColl = listofContacts.ToObject<StatusCollection[]>();

                                foreach (StatusCollection taskDetails in taskColl.OrderBy(x => x.name))
                                {
                                    ComboboxItem item = new ComboboxItem();
                                    item.Text = taskDetails.name;
                                    item.Value = taskDetails.id;
                                    cmbBoxListOfContacts.Items.Add(item);
                                }
                            }

                            fromPostEmailContent.salesUnit = leadSalesUnit;
                            fromPostEmailContent.salesUnitId = leadSalesUnitID;

                            lblSalesUnit.Text = leadSalesUnit;

                            textBoxFirstName.Text = firstName;
                            txtBoxLastName.Text = lastName;
                            txtboxPhone.Text = phonenumber;
                            txtboxSkype.Text = skype;
                            txtboxEmail.Text = email;
                            textBoxWebsite.Text = siteurl;

                            lblCompnayClinetName.Text = txtboxCompanyName.Text + " / " + firstName + " " + lastName;
                            txtBoxDescription.Text = string.Empty;
                            rdButtonClient.IsChecked = false;
                            rdButtonLead.IsChecked = false;

                            checkBoxNewTask.IsChecked = false;
                            cmbboxTasksRemainder.IsEnabled = true;
                            cmbBoxListOfContacts.IsEnabled = true;

                            if (fromPostEmailContent.contextSelection == "AddClientContactExist")
                                chBNewContact.IsChecked = true;
                            else
                                chBNewContact.IsChecked = false;

                            txtBoxTitle.Text = string.Empty;
                            cmbBoxTaskType.SelectedIndex = 0;

                            // int relatedIndex = cmbBoxRelatedTo.FindString(cmb.Text.Trim()); // = Convert.ToInt32(cmb.Value);
                            int relatedIndex = cmbBoxRelatedTo.Items.IndexOf(cmb.Text.Trim());                          
                            cmbBoxRelatedTo.SelectedIndex = relatedIndex;
                            cmbBoxRelatedTo.IsEnabled = false;

                            if (string.IsNullOrEmpty(fromPostEmailContent.clientContactEmail) && string.IsNullOrEmpty(fromPostEmailContent.clientContactFirstName))
                            {
                                txtBoxContactEmail.Text = "";
                                txtBoxContactFirstName.Text = "";
                                txtBoxContactLastName.Text = "";
                                txtBoxContactPhone.Text = "";
                                txtBoxContactSkype.Text = "";
                            }

                            ClearStatusCombo();

                            if (clientRLead == "lead")
                            {
                                rdButtonLead.IsChecked = true;
                                rdButtonClient.IsChecked = false;
                                rdButtonLead.IsEnabled = true;
                                rdButtonClient.IsEnabled = false;

                                if(fromPostEmailContent.leadStatus.Count() == 0)
                                {
                                    //get status from service
                                    dynamic returnValueStatus = GetServiceCall("getleadstatus", "", "GET");
                                    bool bStatusVal = (((Newtonsoft.Json.Linq.JValue)returnValueStatus.success).Value == null) ? false : Convert.ToBoolean(((Newtonsoft.Json.Linq.JValue)returnValueStatus.success).Value);

                                    if (bStatusVal)
                                    {
                                        var sampe = (Newtonsoft.Json.Linq.JArray)returnValueStatus.result;

                                        StatusCollection[] statusColl = sampe.ToObject<StatusCollection[]>();

                                        fromPostEmailContent.leadStatus.Clear();
                                        foreach (StatusCollection statusDetails in statusColl.OrderBy(x => x.name))
                                        {
                                            ComboboxItem item = new ComboboxItem();
                                            item.Text = statusDetails.name;
                                            item.Value = statusDetails.id;
                                            fromPostEmailContent.leadStatus.Add(item);
                                        }
                                    }
                                    else
                                    {
                                        string errorMessage = (((Newtonsoft.Json.Linq.JValue)returnValueStatus.error.message).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnValueStatus.error.message).Value);
                                        //MessageBox.Show(errorMessage, "IUIH", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }                                   
                                }
                                FillStatusCombo(fromPostEmailContent.leadStatus);
                            }
                            else
                            {
                                rdButtonLead.IsChecked = false;
                                rdButtonClient.IsEnabled = true;
                                rdButtonLead.IsEnabled = false;
                                rdButtonClient.IsChecked = true;

                                if(fromPostEmailContent.clientStatus.Count() == 0)
                                {
                                    //get client status from service
                                    dynamic returngetclientstatus = GetServiceCall("getclientstatus", "", "GET");
                                    bool bStatusclientstatus = (((Newtonsoft.Json.Linq.JValue)returngetclientstatus.success).Value == null) ? false : Convert.ToBoolean(((Newtonsoft.Json.Linq.JValue)returngetclientstatus.success).Value);

                                    if (bStatusclientstatus)
                                    {
                                        var sampe = (Newtonsoft.Json.Linq.JArray)returngetclientstatus.result;

                                        StatusCollection[] statusColl = sampe.ToObject<StatusCollection[]>();
                                        fromPostEmailContent.clientStatus.Clear();
                                        foreach (StatusCollection statusDetails in statusColl.OrderBy(x => x.name))
                                        {
                                            ComboboxItem item = new ComboboxItem();
                                            item.Text = statusDetails.name;
                                            item.Value = statusDetails.id;
                                            fromPostEmailContent.clientStatus.Add(item);
                                        }
                                    }
                                    else
                                    {
                                        string errorMessage = (((Newtonsoft.Json.Linq.JValue)returngetclientstatus.error.message).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returngetclientstatus.error.message).Value);
                                        //MessageBox.Show(errorMessage, "IUIH", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                }

                                FillStatusCombo(fromPostEmailContent.clientStatus);
                            }

                            if (!string.IsNullOrEmpty(fromPostEmailContent.senderName) && (fromPostEmailContent.contextSelection == "NewLead" || fromPostEmailContent.contextSelection == "NewClient"))
                            {
                                textBoxContents.Text = string.Empty;
                                rchboxAttachment.Text = string.Empty;
                            }

                            if (!string.IsNullOrEmpty(status))
                            {
                                cmboxStatus.SelectedItem = cmboxStatus.Items.Cast<ComboboxItem>().Where(x => x.Value.ToString() == status).FirstOrDefault();
                            }
                        }
                        else
                        {
                            string errorMessage = (((Newtonsoft.Json.Linq.JValue)returnValue.error.message).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnValue.error.message).Value);
                            MessageBox.Show(errorMessage, "IUIH", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }

            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "IUIH", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        //private void rdButtonLead_Click(object sender, RoutedEventArgs e)
        //{

        //}

        private void chkboxContact_Click(object sender, RoutedEventArgs e)
        {
            if (chkboxContact.IsChecked == true)
            {
                grpBoxContact.Visibility = Visibility.Visible;
                grpBoxLeadClient.Visibility = Visibility.Hidden;
            }
            else
            {
                grpBoxContact.Visibility = Visibility.Hidden;
                grpBoxLeadClient.Visibility = Visibility.Visible;
            }
        }

        private void cmbBoxListOfContacts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ComboBox cmbBoxListOfContactsC = (ComboBox)sender;
                if (cmbBoxListOfContactsC.SelectedIndex > 0)
                {
                    ComboboxItem cmb = (ComboboxItem)cmbBoxListOfContactsC.SelectedItem;
                    ContactDetailBind(cmb);
                }
                else if (cmbBoxListOfContacts.SelectedIndex == 0 && !chkboxContact.IsChecked == true)
                {
                    txtBoxContactFirstName.Text = string.Empty;
                    txtBoxContactLastName.Text = string.Empty;
                    txtBoxContactEmail.Text = string.Empty;
                    txtBoxContactPhone.Text = string.Empty;
                    txtBoxContactSkype.Text = string.Empty;
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "IUIH", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void cmbBoxListOfContacts_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                foreach (ComboboxItem cmb in cmbBoxListOfContacts.Items)
                {
                    if (cmb.Text.ToLower().TrimStart().TrimEnd() == cmbBoxListOfContacts.Text.ToLower().TrimStart().TrimEnd() && cmbBoxListOfContacts.SelectedIndex != 0)
                    {
                        ContactDetailBind(cmb);
                        break;
                    }
                    else if (cmbBoxListOfContacts.SelectedIndex == 0 && !chkboxContact.IsChecked == true)
                    {
                        txtBoxContactFirstName.Text = string.Empty;
                        txtBoxContactLastName.Text = string.Empty;
                        txtBoxContactEmail.Text = string.Empty;
                        txtBoxContactPhone.Text = string.Empty;
                        txtBoxContactSkype.Text = string.Empty;
                    }
                }
            }
        }

        private void chBNewContact_Click(object sender, RoutedEventArgs e)
        {
            txtBoxContactFirstName.Text = string.Empty;
            txtBoxContactLastName.Text = string.Empty;
            txtBoxContactEmail.Text = string.Empty;
            txtBoxContactPhone.Text = string.Empty;
            txtBoxContactSkype.Text = string.Empty;

            if (chBNewContact.IsChecked == true)
            {
                if (cmbBoxListOfContacts.Items.Count > 0)
                    cmbBoxListOfContacts.SelectedIndex = 0;
                cmbBoxListOfContacts.IsEnabled = false;
            }
            else
            {
                if (cmbBoxListOfContacts.Items.Count > 0)
                    cmbBoxListOfContacts.SelectedIndex = 0;

                cmbBoxListOfContacts.IsEnabled = true;
            }
        }

        private void checkBoxNewTask_Click(object sender, RoutedEventArgs e)
        {
            txtBoxTitle.Text = string.Empty;
            cmbBoxTaskType.SelectedIndex = 0;
            cmbBoxRelatedTo.SelectedIndex = 0;
            txtBoxDescription.Text = string.Empty;
            dateTimeDueDate.Value = DateTime.Now;
            dateTimeRemainder.Value = DateTime.Now;

            if (checkBoxNewTask.IsChecked == true)
            {
                cmbboxTasksRemainder.SelectedIndex = 0;
                cmbboxTasksRemainder.IsEnabled = false;
            }
            else
            {
                cmbboxTasksRemainder.SelectedIndex = 0;
                cmbboxTasksRemainder.IsEnabled = true;
            }
        }

        private void cmbboxTasksRemainder_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ComboBox cmbboxTasksRemainderC = (ComboBox)sender;
                if (cmbboxTasksRemainderC.SelectedIndex > 0)
                {
                    ComboboxItem cmb = (ComboboxItem)cmbboxTasksRemainderC.SelectedItem;
                    RemainderDetailBind(cmb);
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "IUIH", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void cmbboxTasksRemainder_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                // e.SuppressKeyPress = true;
                foreach (ComboboxItem cmb in cmbboxTasksRemainder.Items)
                {
                    if (cmb.Text.ToLower().TrimStart().TrimEnd() == cmbboxTasksRemainder.Text.ToLower().TrimStart().TrimEnd())
                    {
                        RemainderDetailBind(cmb);
                        break;
                    }
                }

            }
        }

        private void RemainderDetailBind(ComboboxItem cmb)
        {
            txtBoxTitle.Text = string.Empty;
            txtBoxDescription.Text = string.Empty;

            string taskDueDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm");
            string taskRemianderDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm");

            ClearRelateCombo();

            cmbBoxTaskType.SelectedIndex = 0;
            var postData = cmb != null ? "taskid=" + cmb.Value.ToString() : "taskid = 0";

            postData = postData+ "&uid=" + fromPostEmailContent.loginUniqueKey;

            dynamic returnValue = GetServiceCall("get_task_data", postData, "POST");

            if (returnValue != null)
            {
                bool bStatuss = (((Newtonsoft.Json.Linq.JValue)returnValue.success).Value == null) ? false : Convert.ToBoolean(((Newtonsoft.Json.Linq.JValue)returnValue.success).Value);

                if (bStatuss)
                {
                    checkBoxNewTask.IsChecked = false;

                    //{ "success":true,"result":{ "0":{ "staffid":"2"},"starttime":"30\/06\/2017 17:45","startdate":"30\/06\/2017 17:45","duedate":"30\/06\/2017 17:45","notifytime":"30\/06\/2017 17:51","addedfrom":"2","rel_id":"l_512","rel_type":"lead"} }

                    string title = (((Newtonsoft.Json.Linq.JValue)returnValue.result.name).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnValue.result.name).Value);
                    string taskType = (((Newtonsoft.Json.Linq.JValue)returnValue.result.priority).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnValue.result.priority).Value);

                    string relatedTo = (((Newtonsoft.Json.Linq.JValue)returnValue.result.rel_id).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnValue.result.rel_id).Value);
                    taskDueDate = (((Newtonsoft.Json.Linq.JValue)returnValue.result.duedate).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnValue.result.duedate).Value);

                    taskRemianderDate = (((Newtonsoft.Json.Linq.JValue)returnValue.result.notifytime).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnValue.result.notifytime).Value);
                    string description = (((Newtonsoft.Json.Linq.JValue)returnValue.result.description).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnValue.result.description).Value);

                    txtBoxTitle.Text = title;
                    txtBoxDescription.Text = description;

                    int taskTypeID = 0;

                    if (taskType == "-1")
                        taskTypeID = 0;
                    else
                        taskTypeID = (string.IsNullOrEmpty(taskType)) ? 0 : Convert.ToInt32(taskType);

                    if (taskTypeID != 0)
                    {
                        var sample = cmbBoxTaskType.Items[taskTypeID].ToString();
                        cmbBoxTaskType.SelectedIndex = cmbBoxTaskType.Items.IndexOf(sample);
                    }

                    if (!string.IsNullOrEmpty(relatedTo))
                    {
                        object selectItm = cmbBoxRelatedTo.Items.Cast<ComboboxItem>().Where(x => x.Value.ToString() == relatedTo).FirstOrDefault();

                        if (selectItm != null)
                            cmbBoxRelatedTo.SelectedItem = selectItm;
                        else
                            cmbBoxRelatedTo.SelectedItem = 0;
                    }

                    if (!string.IsNullOrEmpty(taskDueDate))
                    {
                        DateTime oDate = DateTime.ParseExact(taskDueDate, "dd/MM/yyyy HH:mm", null);
                        dateTimeDueDate.Value = oDate;
                    }

                    if (!string.IsNullOrEmpty(taskRemianderDate))
                    {
                        DateTime oDate = DateTime.ParseExact(taskRemianderDate, "dd/MM/yyyy HH:mm", null);
                        dateTimeRemainder.Value = oDate;
                    }
                }
                else
                {
                    string errorMessage = (((Newtonsoft.Json.Linq.JValue)returnValue.error.message).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnValue.error.message).Value);
                    MessageBox.Show(errorMessage, "IUIH", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnAttachment_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                rchboxAttachment.Text = string.Empty;
                string sFileNameList = string.Empty;

                if (fromPostEmailContent.filesAttachedFromEmail != null)
                    sFileNameList = fromPostEmailContent.filesAttachedFromEmail.Replace("|", " , ");
                System.Windows.Forms.OpenFileDialog openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
                    openFileDialog1.Multiselect = true;
                openFileDialog1.CheckFileExists = true;
                openFileDialog1.CheckPathExists = true;

                openFileDialog1.DefaultExt = "txt";
                openFileDialog1.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog1.FilterIndex = 2;
                openFileDialog1.RestoreDirectory = true;

                openFileDialog1.ReadOnlyChecked = true;
                openFileDialog1.ShowReadOnly = true;

                // sInstalledPath = sInstalledPath+@"\UploadFiles\";
                string sPath = System.IO.Path.Combine(fromPostEmailContent.installationPath, fromPostEmailContent.fileUploadFolder);

                if (!Directory.Exists(sPath))
                {
                    Directory.CreateDirectory(sPath);
                }

                if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    for (int icnt = 0; icnt < openFileDialog1.FileNames.Count(); icnt++)
                    {
                        string snewPath = System.IO.Path.Combine(sPath, openFileDialog1.SafeFileNames[icnt]);

                        if (File.Exists(snewPath))
                            File.Delete(snewPath);

                        fromPostEmailContent.attachedFilePath.Add(snewPath);
                        File.Copy(openFileDialog1.FileNames[icnt], snewPath);

                        if (string.IsNullOrEmpty(sFileNameList))
                            sFileNameList = openFileDialog1.SafeFileNames[icnt];
                        else
                            sFileNameList = sFileNameList + " , " + openFileDialog1.SafeFileNames[icnt];
                    }
                }

                if (!string.IsNullOrEmpty(sFileNameList))
                {
                    rchboxAttachment.Visibility = Visibility.Visible;
                    rchboxAttachment.Text = sFileNameList;
                    fromPostEmailContent.filesAttachedFromEmail = sFileNameList;
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "IUIH", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnDeleteAttachment_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult dialogResult = MessageBox.Show("Are you sure you want to remove all the attachments?", "IUIH", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (dialogResult == MessageBoxResult.Yes)
            {
                fromPostEmailContent.attachedFilePath.Clear();

                rchboxAttachment.Text = string.Empty;
                rchboxAttachment.Visibility = Visibility.Visible;
                fromPostEmailContent.filesAttachedFromEmail = string.Empty;
            }
        }

       

        private void buttonSave_Click(object sender, RoutedEventArgs e)
        {


            System.Text.RegularExpressions.Regex rEmail = new System.Text.RegularExpressions.Regex(@"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$");

            if (txtboxEmail.Text.Length > 0 && txtboxEmail.Text.Trim().Length != 0)
            {
                if (!rEmail.IsMatch(txtboxEmail.Text.Trim()))
                {
                    MessageBox.Show("Please enter valid email address", "IUIH", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txtboxEmail.SelectAll();
                    return;
                }
            }

            if (string.IsNullOrEmpty(txtBoxTitle.Text.Trim()) && tabMain.SelectedIndex == 1)
            {
                MessageBox.Show("Please enter task title", "IUIH", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            else if (cmbBoxTaskType.SelectedIndex == 0 && tabMain.SelectedIndex == 1)
            {
                MessageBox.Show("Please select task type", "IUIH", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            else if (string.IsNullOrEmpty(txtBoxDescription.Text.Trim()) && tabMain.SelectedIndex == 1)
            {
                MessageBox.Show("Please enter task description", "IUIH", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            //else if (string.IsNullOrEmpty(textBoxContents.Text.Trim()) && tabMain.SelectedIndex == 2)
            //{
            //    MessageBox.Show("Please enter mail content", "IUIH", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return;
            //}
            //else if (string.IsNullOrEmpty(txtboxNotes.Text.Trim()) && tabMain.SelectedIndex == 2)
            //{
            //    MessageBox.Show("Please enter comments", "IUIH", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return;
            //}
            if (string.IsNullOrEmpty(txtboxCompanyName.Text.Trim()) && tabMain.SelectedIndex == 0)
            {
                MessageBox.Show("Please enter company name", "IUIH", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            else if (cmboxStatus.SelectedIndex == 0 && tabMain.SelectedIndex == 0)
            {
                MessageBox.Show("Please select status", "IUIH", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }


            if (rdButtonClient.IsChecked == false && rdButtonLead.IsChecked == false && txtboxCompanyName.Text.Trim() == "" && cmbCompanyName.SelectedIndex == 0 && checkBoxNewTask.IsChecked == false)
            {
                MessageBox.Show("Please fill the mandatory fields.", "IUIH", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            if (chBNewContact.IsChecked == true || cmbBoxListOfContacts.SelectedIndex != 0)// && chkboxContact.Checked == true)
            {
                if (string.IsNullOrEmpty(txtBoxContactFirstName.Text.Trim()))
                {
                    MessageBox.Show("Please enter contact first name", "IUIH", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (string.IsNullOrEmpty(txtBoxContactLastName.Text.Trim()))
                {
                    MessageBox.Show("Please enter contact last name", "IUIH", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (string.IsNullOrEmpty(txtBoxContactEmail.Text.Trim()))
                {
                    MessageBox.Show("Please enter contact email address", "IUIH", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
            }

            ProcessShow();
            int maxItems = 50;
            pbProcess.Minimum = 1;
            pbProcess.Maximum = 100;
           
            _bw.RunWorkerAsync(maxItems);
                     

            //Task task = Task.Factory.StartNew(() =>
            //{
            //    //_bw.RunWorkerAsync();
            //    //int maxItems = 50;
            //    //pbProcess.Minimum = 1;
            //    //pbProcess.Maximum = 100;

            //    //  StatusTextBox.Text = "Starting...";
            //    _bw.RunWorkerAsync(50);

            //});
        }

        private void Save(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            int? maxItems = e.Argument as int?;
            for (int i = 1; i <= maxItems.GetValueOrDefault(); ++i)
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }

                Thread.Sleep(200);
                worker.ReportProgress(i);

                //item added
            }

            //this.Invoke(new System.Windows.Forms.MethodInvoker(async delegate ()
            Dispatcher.Invoke(DispatcherPriority.Normal, new Action(async delegate()
            {
                try
                {
                    string selectedCompanyText = string.Empty;
                    string istype = string.Empty;

                    if (rdButtonClient.IsChecked == true)
                        istype = "client";
                    else if (rdButtonLead.IsChecked == true)
                        istype = "lead";

                    LeadDetails objLeadDetails = new LeadDetails();
                    objLeadDetails.companyName = txtboxCompanyName.Text.Trim();
                    objLeadDetails.firstName = textBoxFirstName.Text.Trim();
                    objLeadDetails.lastName = txtBoxLastName.Text.Trim();
                    objLeadDetails.clientName = txtBoxLastName.Text.Trim();
                    objLeadDetails.contents = textBoxContents.Text.Trim();
                    objLeadDetails.notes = txtboxNotes.Text.Trim();
                    objLeadDetails.phone = txtboxPhone.Text.Trim();
                    objLeadDetails.salesUnit = lblSalesUnit.Text.Trim();
                    objLeadDetails.skype = txtboxSkype.Text.Trim();
                    objLeadDetails.email = txtboxEmail.Text.Trim();
                    ComboboxItem cmboxStatu = (ComboboxItem)cmboxStatus.SelectedItem;
                    if (cmboxStatu != null)
                        objLeadDetails.statusId = cmboxStatu.Value.ToString();
                    objLeadDetails.istype = istype;
                    objLeadDetails.taskTitle = txtBoxTitle.Text.Trim();
                    objLeadDetails.taskDescription = txtBoxDescription.Text.Trim();
                    objLeadDetails.taskType = Convert.ToString(cmbBoxTaskType.SelectedIndex);

                    DateTime dtn = (dateTimeDueDate.Value == null)?DateTime.Now : dateTimeDueDate.Value.Value;                   
                    objLeadDetails.dueDate = dtn.ToString("yyyy-MM-dd HH:mm");
                    //dateTimeDueDate.Value.Value.Year + "-" + ((dateTimeDueDate.Value.Value.Month.ToString().Length == 1)? (dateTimeDueDate.Value.Value.Month.ToString()+"0"): dateTimeDueDate.Value.Value.Month.ToString() )+ "-" + dateTimeDueDate.Value.Value.Day + " " + dateTimeDueDate.Value.Value.Hour + ":" + dateTimeDueDate.Value.Value.Minute;
                    //dateTimeDueDate.Value.ToString();//.ToString("yyyy-MM-dd HH:mm");
                    ComboboxItem cmboxRelated = (ComboboxItem)cmbBoxRelatedTo.SelectedItem;
                    if (cmboxRelated != null)
                        objLeadDetails.relatedTo = cmboxRelated.Value.ToString();
                    objLeadDetails.webSiteUrl = textBoxWebsite.Text.Trim();
                    if (cmbboxTasksRemainder.SelectedIndex == 0 && checkBoxNewTask.IsChecked == true)
                    {
                        //new task
                        objLeadDetails.isNewTask = "true";
                        objLeadDetails.taskID = "0";
                    }
                    else
                    {
                        objLeadDetails.isNewTask = "false";
                        ComboboxItem cmboxReminder = (ComboboxItem)cmbboxTasksRemainder.SelectedItem;
                        if (cmboxReminder != null)
                            objLeadDetails.taskID = cmboxReminder.Value.ToString();
                    }

                    DateTime dtnRem = (dateTimeRemainder.Value == null) ? DateTime.Now : dateTimeRemainder.Value.Value;
                    objLeadDetails.reminder = dtnRem.ToString("yyyy-MM-dd HH:mm");
                    //dateTimeRemainder.Value.Value.Year + "-" +( (dateTimeRemainder.Value.Value.Month.ToString().Length == 1) ? (dateTimeRemainder.Value.Value.Month.ToString() + "0") : dateTimeRemainder.Value.Value.Month.ToString()) + "-" + dateTimeRemainder.Value.Value.Day + " " + dateTimeRemainder.Value.Value.Hour + ":" + dateTimeRemainder.Value.Value.Minute;
                    //dateTimeRemainder.Value.ToString();//ToString("yyyy-MM-dd HH:mm");

                    string uploadedFileName = string.Empty;

                    foreach (string attachedPath in fromPostEmailContent.attachedFilePath)
                    {
                        string sPath = System.IO.Path.Combine(fromPostEmailContent.installationPath, fromPostEmailContent.fileUploadFolder);
                        string fileName = attachedPath.Replace(sPath, "").Replace("\\", "");

                        string serviceMethodPath = System.IO.Path.Combine(fromPostEmailContent.serviceURL, "file");

                        using (System.Net.WebClient Client = new System.Net.WebClient())
                        {
                            System.Net.ServicePointManager.Expect100Continue = false;

                            Client.Headers.Add("Content-Type", "binary/octet-stream");

                            byte[] result = Client.UploadFile(serviceMethodPath, "POST", attachedPath);

                            string requestReturn = System.Text.Encoding.UTF8.GetString(result, 0, result.Length);

                            JObject jsonObj = JObject.Parse(requestReturn);

                            bool resultSuccess = (bool)(jsonObj["success"]);

                            if (resultSuccess)
                            {
                                string resultvalue = (string)jsonObj["result"];

                                if (string.IsNullOrEmpty(uploadedFileName))
                                    uploadedFileName = resultvalue;
                                else
                                    uploadedFileName = uploadedFileName + "," + resultvalue;
                            }
                        }
                    }

                    ComboboxItem companySelectedItem = (ComboboxItem)cmbCompanyName.SelectedItem;
                    selectedCompanyText = string.Empty;
                    var selectedCompany = companySelectedItem != null ? companySelectedItem.Text.Trim() : string.Empty;
                    bool isNewRecord = false;
                    if (cmbCompanyName.SelectedIndex == 0)
                        selectedCompanyText = string.Empty;
                    else
                        selectedCompanyText = txtboxCompanyName.Text.Trim();

                    if (cmbCompanyName.SelectedIndex == 0)//|| (selectedCompany != txtboxCompanyName.Text.Trim()))
                    {
                        objLeadDetails.id = "0";
                        isNewRecord = true;
                    }
                    else
                    {
                        ComboboxItem cmbName = (ComboboxItem)cmbCompanyName.SelectedItem;
                        objLeadDetails.id = cmbName != null ? Convert.ToString(cmbName.Value) : "0";
                        isNewRecord = false;
                    }

                    string contactID = "0";

                    if (cmbBoxListOfContacts.SelectedIndex == 0 && chBNewContact.IsChecked == true)
                        contactID = "0";
                    else
                    {
                        ComboboxItem cmbBoxListOfContactsC = (ComboboxItem)cmbBoxListOfContacts.SelectedItem;
                        if (cmbBoxListOfContactsC != null)
                            contactID = cmbBoxListOfContactsC.Value.ToString();
                    }

                    string tousertype = "";

                    if (rdBtnPrivate.IsChecked == true)
                        tousertype = "0";
                    else if (rdBtnDevTeam.IsChecked == true)
                        tousertype = "3";
                    else if (rdBtnMyFranchise.IsChecked == true)
                        tousertype = "1";
                    else if (rdBtnFranchisee.IsChecked == true)
                        tousertype = "5";
                    else if (rdBtnTSUSA.IsChecked == true)
                        tousertype = "2";
                    
                    var postData = "companyName=" + objLeadDetails.companyName;
                    postData += "&salesUnit=" + fromPostEmailContent.salesUnitId;
                    postData += "&firstname=" + objLeadDetails.firstName;
                    postData += "&lastname=" + objLeadDetails.lastName;
                    postData += "&email=" + objLeadDetails.email;
                    postData += "&phone=" + objLeadDetails.phone;
                    postData += "&skype=" + objLeadDetails.skype;
                    postData += "&notes=" + objLeadDetails.notes;
                    postData += "&content=" + objLeadDetails.contents;
                    postData += "&statusId=" + objLeadDetails.statusId;
                    postData += "&uid=" + fromPostEmailContent.loginUniqueKey;

                    if (objLeadDetails.id != "0")
                        postData += "&id=" + objLeadDetails.id;

                    postData += "&attachment=" + uploadedFileName;
                    postData += "&istype=" + istype;
                    postData += "&taskid=" + objLeadDetails.taskID;
                    postData += "&taskName=" + objLeadDetails.taskTitle;
                    postData += "&priorityType=" + objLeadDetails.taskType;
                    postData += "&taskDateTime=" + objLeadDetails.dueDate;
                    postData += "&notifyDateTime=" + objLeadDetails.reminder;
                    postData += "&taskdescription=" + objLeadDetails.taskDescription;
                    postData += "&relateId=" + objLeadDetails.relatedTo;
                    postData += "&siteurl=" + objLeadDetails.webSiteUrl;
                    postData += "&contact_id=" + contactID;
                    postData += "&conact_first_name=" + txtBoxContactFirstName.Text.Trim();
                    postData += "&contact_last_name=" + txtBoxContactLastName.Text.Trim();
                    postData += "&contact_email=" + txtBoxContactEmail.Text.Trim();
                    postData += "&contact_phone=" + txtBoxContactPhone.Text.Trim();
                    postData += "&contact_skype=" + txtBoxContactSkype.Text.Trim();
                    postData += "&tousertype=" + tousertype;

                    if (fromPostEmailContent.listCCContacts.Count > 0)
                    {
                        var json = JsonConvert.SerializeObject(fromPostEmailContent.listCCContacts);

                        postData += "&contacts=" + json;
                    }

                    await Task.Run(() =>
                    {
                        dynamic returnValue = GetServiceCall("lead_new", postData, "POST");

                        if (returnValue != null)
                        {
                            bool bStatuss = (((Newtonsoft.Json.Linq.JValue)returnValue.success).Value == null) ? false : Convert.ToBoolean(((Newtonsoft.Json.Linq.JValue)returnValue.success).Value);

                            if (bStatuss)
                            {
                                //this.Invoke(new MethodInvoker(delegate ()
                                Dispatcher.Invoke(DispatcherPriority.Normal, new Action(delegate ()
                                {
                                    //string[] sReturnVal = new string[10];
                                    ////(returnValue.message == null) ? "" : (returnValue.message.ToString().Replace("[", "").Replace("]", "").Trim());

                                    //if(returnValue.message != null && returnValue.message.Contains(","))
                                    //{
                                    //    sReturnVal = returnValue.message.Split(",").Replace("[", "").Replace("]", "").Trim();
                                    //}
                                    //else
                                    //    sReturnVal[0] = (returnValue.message == null) ? "" : (returnValue.message.ToString().Replace("[", "").Replace("]", "").Trim());

                                    string[] sReturnVal = returnValue.message.ToObject<string[]>();
                                    string resultMessage = "";
                                    //"\"No changes in lead\",\r\n  \"Already added for this contact to this lead...!\""

                                    foreach (string strRetVal in sReturnVal)
                                    {
                                        string successMessage = string.Empty;
                                        if (strRetVal != "" && strRetVal.ToUpper().Contains("NO CHANGES") && (strRetVal.Contains(",") == false))
                                            successMessage = strRetVal;
                                        if (strRetVal != "" && strRetVal.ToUpper().Contains("NO CHANGES") && (strRetVal.Contains(",") == true) && strRetVal.ToUpper().Contains("ALREADY ADDED"))
                                            successMessage = "Details already exists. Kindly check.";
                                        else if (fromPostEmailContent.contextSelection == "PostEmailContent")
                                            successMessage = "Content posted successfully";
                                        else if (strRetVal != "" && strRetVal.ToUpper().Contains("LEAD WAS UPDATED SUCCESSFULLY"))
                                            successMessage = "Lead was updated successfully";
                                        else if (strRetVal != "" && strRetVal.ToUpper().Contains("CLIENT WAS UPDATED SUCCESSFULLY"))
                                            successMessage = "Client was updated successfully";
                                        else if (strRetVal != "" && strRetVal.ToUpper().Contains("TASK WAS UPDATED SUCCESSFULLY"))
                                            successMessage = "Task was updated successfully";
                                        else if (strRetVal != "" && strRetVal.ToUpper().Contains("LEAD WAS ADDED SUCCESSFULLY"))
                                            successMessage = "Lead was added successfully";
                                        else if (strRetVal != "" && strRetVal.ToUpper().Contains("CLIENT WAS ADDED SUCCESSFULLY"))
                                            successMessage = "Client was added successfully";
                                        else if (strRetVal != "" && strRetVal.ToUpper().Contains("TASK WAS ADDED SUCCESSFULLY"))
                                            successMessage = "Task was added successfully";
                                        else if (strRetVal != "" && strRetVal.ToUpper().Contains("ALREADY ADDED FOR THIS CONTACT TO THIS LEAD"))
                                            successMessage = "Contact already exists";
                                        else
                                        {
                                            successMessage = strRetVal;
                                            //if (tabMain.SelectedIndex == 0)
                                            //    successMessage = "Lead/Client " + (isNewRecord ? "saved" : "updated") + " successfully";
                                            //else if (tabMain.SelectedIndex == 1)
                                            //    successMessage = "Task  " + (isNewRecord ? "saved" : "updated") + "  successfully";
                                            //else if (tabMain.SelectedIndex == 2)
                                            //    successMessage = "Mail content or attachment/s saved successfully";
                                            ////else if (tabMain.SelectedIndex == 3)
                                            ////    successMessage = "Client contact saved successfully";
                                        }

                                        if (resultMessage == "")
                                            resultMessage = successMessage;
                                        else
                                            resultMessage = resultMessage +"\n"+successMessage;
                                    }


                                    if (resultMessage != "")
                                        MessageBox.Show(resultMessage, "IUIH", MessageBoxButton.OK, MessageBoxImage.Information);

                                    //string successMessage = string.Empty;
                                    //if (sReturnVal != "" && sReturnVal.ToUpper().Contains("NO CHANGES") && (sReturnVal.Contains(",") == false))
                                    //    successMessage = sReturnVal;
                                    //if (sReturnVal != "" && sReturnVal.ToUpper().Contains("NO CHANGES") && (sReturnVal.Contains(",") == true) && sReturnVal.ToUpper().Contains("ALREADY ADDED"))
                                    //    successMessage = "Details already exists. Kindly check.";
                                    //else if(fromPostEmailContent.contextSelection == "PostEmailContent")
                                    //    successMessage = "Content posted successfully";
                                    //else if (sReturnVal != "" && sReturnVal.ToUpper().Contains("LEAD WAS UPDATED SUCCESSFULLY"))
                                    //    successMessage = "Lead was updated successfully";
                                    //else if (sReturnVal != "" && sReturnVal.ToUpper().Contains("CLIENT WAS UPDATED SUCCESSFULLY"))
                                    //    successMessage = "Client was updated successfully";
                                    //else if (sReturnVal != "" && sReturnVal.ToUpper().Contains("TASK WAS UPDATED SUCCESSFULLY"))
                                    //    successMessage = "Task was updated successfully";
                                    //else if (sReturnVal != "" && sReturnVal.ToUpper().Contains("LEAD WAS ADDED SUCCESSFULLY"))
                                    //    successMessage = "Lead was added successfully";
                                    //else if (sReturnVal != "" && sReturnVal.ToUpper().Contains("CLIENT WAS ADDED SUCCESSFULLY"))
                                    //    successMessage = "Client was added successfully";
                                    //else if (sReturnVal != "" && sReturnVal.ToUpper().Contains("TASK WAS ADDED SUCCESSFULLY"))
                                    //    successMessage = "Task was added successfully";
                                    //else
                                    //{
                                    //    if (tabMain.SelectedIndex == 0)
                                    //        successMessage = "Lead/Client " + (isNewRecord ? "saved" : "updated") + " successfully";
                                    //    else if (tabMain.SelectedIndex == 1)
                                    //        successMessage = "Task  " + (isNewRecord ? "saved" : "updated") + "  successfully";
                                    //    else if (tabMain.SelectedIndex == 2)
                                    //        successMessage = "Mail content or attachment/s saved successfully";
                                    //    //else if (tabMain.SelectedIndex == 3)
                                    //    //    successMessage = "Client contact saved successfully";
                                    //}

                                    //if (successMessage != "")
                                    //    MessageBox.Show(successMessage, "IUIH", MessageBoxButton.OK, MessageBoxImage.Information);

                                }));

                                fromPostEmailContent.clientContactEmail = "";
                                fromPostEmailContent.clientContactFirstName = "";
                                fromPostEmailContent.clientContactLastName = "";
                            }
                            else
                            {
                                var errorMessage = returnValue.message.ToString().Replace("[", "").Replace("]", "");
                                MessageBox.Show(errorMessage, "IUIH", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                        }
                        else
                            MessageBox.Show("Server error. Please try after sometime.", "IUIH", MessageBoxButton.OK, MessageBoxImage.Error);

                        //this.Invoke(new MethodInvoker(delegate ()
                        Dispatcher.Invoke(DispatcherPriority.Normal, new Action(delegate ()
                        {
                            ClearValues(fromPostEmailContent, true);
                            BindMasterValues();
                            ProcessHide();                           
                            pbProcess.Value = 0;

                            
                            grpBoxLeadClient.Visibility = Visibility.Visible;
                            grpBoxContact.Visibility = Visibility.Collapsed;
                            //groupBox1.Visibility = Visibility.Visible;

                            rdButtonClient.IsEnabled = true;
                            rdButtonLead.IsEnabled = true;
                            chkboxContact.IsChecked = false;
                            rdButtonLead.IsChecked = false;
                            rdButtonClient.IsChecked = false;
                            //rdButtonLead.IsThreeState = true;
                            //rdButtonClient.IsEnabled = true;
                        }));
                    });
                }
                catch (Exception err)
                {
                    MessageBox.Show(err.Message, "IUIH", MessageBoxButton.OK, MessageBoxImage.Error);
                    ProcessHide();
                    pbProcess.Value = 0;
                }

            }));
        }

        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            try
            {
               MessageBoxResult dialogResult = MessageBox.Show("Are you sure you want to logout?", "IUIH", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (dialogResult == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(fromPostEmailContent.loginUniqueKey))
                        return;

                    var postData = "uid=" + fromPostEmailContent.loginUniqueKey;

                    dynamic returnValue = GetServiceCall("logout", postData, "POST");
                    bool bStatuss = (((Newtonsoft.Json.Linq.JValue)returnValue.success).Value == null) ? false : Convert.ToBoolean(((Newtonsoft.Json.Linq.JValue)returnValue.success).Value);

                    if (bStatuss)
                    {
                        //save the vales entred in UI to registry
                        RegistryKey key = Registry.CurrentUser.OpenSubKey(fromPostEmailContent.registryPath, true);

                        if (key == null)
                            key = Registry.CurrentUser.CreateSubKey(fromPostEmailContent.registryPath);

                        key.SetValue("UserName", "");
                        key.SetValue("Password", "");
                        key.SetValue("SalesUnit", "");
                        key.SetValue("SalesUnitId", "");
                        key.SetValue("Status", "");
                        key.SetValue("LoginUniqueKey", "");
                        key.SetValue("DisplayRequest", "0");
                        key.SetValue("Role", "");
                        key.SetValue("IsAdmin", "0");
                        key.SetValue("AddtionalContact", "");
                        key.Close();

                        MessageBoxResult result = MessageBox.Show("You are logged out successfully.", "IUIH", MessageBoxButton.OK, MessageBoxImage.Information);

                        if (result == MessageBoxResult.Yes)
                        {
                            this.Close();
                        }
                    }
                    else
                    {
                        string errorMessage = (((Newtonsoft.Json.Linq.JValue)returnValue.error.message).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnValue.error.message).Value);
                        MessageBox.Show(errorMessage, "IUIH", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "IUIH", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        //private void MetroWindow_StateChanged(object sender, EventArgs e)
        //{
        //    switch (this.WindowState)
        //    {
        //        //case WindowState.Maximized:
        //        //    MessageBox.Show("Maximized");
        //        //    break;
        //        case WindowState.Minimized:
        //            // MessageBox.Show("Minimized");
        //            //this.min
        //            //this.pa 
        //            break;
        //        case WindowState.Normal:
        //            //MessageBox.Show("Normal");
        //            break;
        //    }
        //}

        private void rdButtonLead_Click_1(object sender, RoutedEventArgs e)
        {
            if (copyFromMail == true && isCopyClientAddress == false && !string.IsNullOrEmpty(fromPostEmailContent.contextSelection))
                SetLeadORClientStatus("LEAD");
            else
            {
                ClearStatusCombo();
                rdButtonLead.IsChecked = true;
                rdButtonClient.IsChecked = false;

                FillStatusCombo(fromPostEmailContent.leadStatus);
            }
        }

        private void rdButtonClient_Click(object sender, RoutedEventArgs e)
        {
            if (copyFromMail == true && isCopyClientAddress == false && !string.IsNullOrEmpty(fromPostEmailContent.contextSelection))
                SetLeadORClientStatus("CLIENT");
            else
            {
                ClearStatusCombo();
                rdButtonLead.IsChecked = false;
                rdButtonClient.IsChecked = true;

                FillStatusCombo(fromPostEmailContent.clientStatus);
            }
        }
    }
}
