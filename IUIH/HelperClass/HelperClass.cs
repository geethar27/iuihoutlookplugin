﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace IUIH
{
    public class EventDetails
    {
        public string eventId { get; set; }
        public string eventTitle { get; set; }
        public string eventDetails { get; set; }
        public string eventImage { get; set; }
        public string eventDate { get; set; }
        public string eventStartTime { get; set; }
        public string eventEndTime { get; set; }
        
        public DateTime eventDateTime
        {
            get { return Convert.ToDateTime(eventDate); }
            set { eventDateTime = Convert.ToDateTime(value);  }
        }

        public DateTime eventStartDateTime
        {
            get { return Convert.ToDateTime(eventStartTime); }
            set { eventStartDateTime = Convert.ToDateTime(value); }
        }

        public DateTime eventEndDateTime
        {
            get { return Convert.ToDateTime(eventEndTime); }
            set { eventEndDateTime = Convert.ToDateTime(value); }
        }
        
        public object documents { get; set; }  
        public string rsvpTotaluser { get; set; }
        public object category { get; set; }
        public object location { get; set; }
        public object instructor { get; set; }
        public bool isExists { get; set; }
      public string eventStatus { get; set; }
        public List<string> categoryLst
        {
            get
            {
                return JsonConvert.DeserializeObject<List<string>>(category.ToString());
            }
            set
            {
                categoryLst = JsonConvert.DeserializeObject<List<string>>(category.ToString());
            }
        }

        public List<string> locationLst
        {
            get
            {
                return JsonConvert.DeserializeObject<List<string>>(location.ToString());
            }
            set
            {
                locationLst = JsonConvert.DeserializeObject<List<string>>(location.ToString());
            }
        }

        public List<string> instructorLst
        {
            get
            {
                return JsonConvert.DeserializeObject<List<string>>(instructor.ToString());
            }
            set
            {
                instructorLst = JsonConvert.DeserializeObject<List<string>>(instructor.ToString());
            }
        }
    }
   
}