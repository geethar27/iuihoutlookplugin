﻿using MahApps.Metro.Controls;
using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace IUIH
{
    /// <summary>
    /// Interaction logic for LogoutWPF.xaml
    /// </summary>
    public partial class LogoutWPF : MetroWindow
    {
        private PostEmailDetails fromPostEmailContent = new PostEmailDetails();

        public LogoutWPF()
        {
            InitializeComponent();
        }

        private string registryPath = ConfigurationManager.AppSettings["RegistryPath"].ToString();

        public LogoutWPF(PostEmailDetails passPostEmailContent)
        {
            InitializeComponent();            

            //fromPostEmailContent = passPostEmailContent;
        }

        private void TabablzControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        //private void buttonSignin_Click(object sender, RoutedEventArgs e)
        //{
        //    //    textBoxUserName.Text = "mettrodev";
        //    //    textBoxPassword.Password = "nawB)Th5axeO1tRMFTaE1uQp";

        //    if ((textBoxUserName.Text == "") || (textBoxPassword.Password == ""))
        //    {
        //        System.Windows.MessageBox.Show("Please enter your credentials", "IUIH", MessageBoxButton.OK, MessageBoxImage.Warning);
        //        return;
        //    }

        //    try
        //    {
        //        ServicePointManager.Expect100Continue = true;
        //        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

        //        //service call for Logout functionality
        //        var request = (HttpWebRequest)WebRequest.Create(fromPostEmailContent.serviceURL);

        //        request.Credentials = new NetworkCredential("username", "password");
        //        string credidentials = fromPostEmailContent.authUserName + ":" + fromPostEmailContent.authPassword; 
        //        var authorization = Convert.ToBase64String(Encoding.Default.GetBytes(credidentials));
        //        request.Headers["Authorization"] = "Basic " + authorization;


        //        var postData = "action=Logout"+ "&username=" + textBoxUserName.Text.Trim() + "&password=" + textBoxPassword.Password.Trim();
        //        var data = System.Text.Encoding.ASCII.GetBytes(postData);

        //        request.Method = "POST";
        //        request.ContentType = "application/x-www-form-urlencoded";
        //        request.ContentLength = data.Length;

        //        using (var stream = request.GetRequestStream())
        //        {
        //            stream.Write(data, 0, data.Length);
        //        }

        //        var response = (HttpWebResponse)request.GetResponse();

        //        //get sevice response
        //        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();               

        //        if (responseString.Contains("<div"))
        //        {
        //            try
        //            {
        //                responseString = responseString.Substring(responseString.IndexOf("{"), responseString.Length - responseString.IndexOf("{"));
        //            }
        //            catch (Exception ex)
        //            {
        //            }
        //        }

        //        dynamic returnResponse = JsonConvert.DeserializeObject(responseString);
        //        bool bStatuss = (((Newtonsoft.Json.Linq.JValue)returnResponse.status).Value == null) ? false : Convert.ToBoolean(((Newtonsoft.Json.Linq.JValue)returnResponse.status).Value);

        //        if (bStatuss)
        //        {
        //            MessageBox.Show("You are successfully logged in", "IUIH", MessageBoxButton.OK, MessageBoxImage.Information);

        //            string username = (((Newtonsoft.Json.Linq.JValue)returnResponse.response.username).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnResponse.response.username).Value);
        //            string userid = (((Newtonsoft.Json.Linq.JValue)returnResponse.response.userid).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnResponse.response.userid).Value);

        //            if (string.IsNullOrEmpty(userid))
        //                return;

        //            //save the vales entred in UI to registry
        //            RegistryKey key = Registry.CurrentUser.OpenSubKey(fromPostEmailContent.registryPath, true);

        //            if (key == null)
        //                key = Registry.CurrentUser.CreateSubKey(fromPostEmailContent.registryPath);

        //            key.SetValue("LogoutUserName", textBoxUserName.Text.Trim());
        //            key.SetValue("Password", textBoxPassword.Password.Trim());
        //            key.SetValue("UserName", username);                    
        //            key.SetValue("LogoutUniqueKey", userid);                   
        //            key.Close();

        //            Ribbon obj = new Ribbon();

        //            this.Close();                   
        //        }
        //        else
        //        {
        //            string errorMessage = (((Newtonsoft.Json.Linq.JValue)returnResponse.error.message).Value == null) ? "" : Convert.ToString(((Newtonsoft.Json.Linq.JValue)returnResponse.error.message).Value);
        //            MessageBox.Show(errorMessage, "IUIH", MessageBoxButton.OK, MessageBoxImage.Error);
        //        }
        //    }
        //    catch (Exception err)
        //    {
        //        MessageBox.Show(err.Message, "IUIH", MessageBoxButton.OK, MessageBoxImage.Error);
        //    }
        //}

        private void buttonOk_Click(object sender, RoutedEventArgs e)
        {

        }

        private void buttonYes_Click(object sender, RoutedEventArgs e)
        {

        }

        private void buttonNO_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}