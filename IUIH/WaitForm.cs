﻿using System;
using System.Threading;
using System.Windows.Forms;
using IUIHSkin.Controls;

namespace IUIH
{
    public partial class WaitForm : MaterialForm
    {
        private readonly MethodInvoker method;

        public WaitForm()
        {

        }
        public WaitForm(MethodInvoker action)
        {
            InitializeComponent();
            method = action;

            this.Load += WaitForm_Load;
        }

        private void WaitForm_Load(object sender, EventArgs e)
        {
            new Thread(() =>
            {
                method.Invoke();
               // InvokeAction(this, Dispose);
            }).Start();
        }

        public static void InvokeAction(Control control, MethodInvoker action)
        {
            if (control.InvokeRequired)
            {
                control.BeginInvoke(action);
            }
            else
            {
                action();
            }
        }
    }
}